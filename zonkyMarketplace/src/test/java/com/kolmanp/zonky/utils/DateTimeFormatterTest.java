package com.kolmanp.zonky.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import com.kolmanp.zonky.TestUtils;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class DateTimeFormatterTest {

	@Test(expected = IllegalArgumentException.class)
	public void testBadInput() {
		new DateTimeFormatter().format(null);
	}
	
	@Test
	public void testFormat() {
		assertEquals(TestUtils.getTestLastUpdateDateString(), new DateTimeFormatter().format(TestUtils.getTestLastUpdateDate()));
	}

}

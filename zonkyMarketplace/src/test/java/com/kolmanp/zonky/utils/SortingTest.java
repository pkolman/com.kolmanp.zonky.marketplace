package com.kolmanp.zonky.utils;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class SortingTest {

	@Test
	public void testSortingFormat() {
		assertEquals("-datePublished", new Sorting("datePublished", Direction.DESC).toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalFeature() {
		new Sorting(null, Direction.DESC);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalDirection() {
		new Sorting("datePublished", null);
	}

}

package com.kolmanp.zonky.utils;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class DirectionTest {

	@Test
	public void testAscStringValue() {
		assertEquals("", Direction.ASC.toString());
	}
	
	@Test
	public void testDescStringValue() {
		assertEquals("-", Direction.DESC.toString());
	}

}

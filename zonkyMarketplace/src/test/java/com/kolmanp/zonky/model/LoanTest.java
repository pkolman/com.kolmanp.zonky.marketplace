package com.kolmanp.zonky.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.kolmanp.zonky.TestUtils;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class LoanTest {

	@Test
	public void testLoanFormat() {
		assertEquals(TestUtils.createTestLoan().toString(), TestUtils.getTestLoanString());
	}
	
}

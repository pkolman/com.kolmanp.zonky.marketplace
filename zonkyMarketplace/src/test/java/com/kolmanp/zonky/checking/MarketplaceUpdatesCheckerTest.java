package com.kolmanp.zonky.checking;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.test.context.junit4.SpringRunner;

import com.kolmanp.zonky.TestUtils;
import com.kolmanp.zonky.services.MarketplaceService;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MarketplaceUpdatesCheckerTest {

	@MockBean
	private MarketplaceService service;
	
    @Autowired
    private MarketplaceUpdatesChecker checker;
    
    @Rule
    public OutputCapture capture =  new OutputCapture();
    
    @Before
    public void setup() {
    		capture.reset();
    }
	
    @Test
	public void testNoLoans() {
        given(service.getNewLoans()).willReturn(Collections.emptyList());
        checker.checkMarketplace();
        
        String output = capture.toString();
		assertThat(output, containsString("New loans: []"));
	}
    
    @Test
	public void testExistingLoan() {
		given(service.getNewLoans()).willReturn(Arrays.asList(TestUtils.createTestLoan()));
        checker.checkMarketplace();
        
        String output = capture.toString();
		assertThat(output, containsString("New loans: [" + TestUtils.getTestLoanString() + "]"));
	}
    
    @Test
	public void testExistingLoans() {
		given(service.getNewLoans()).willReturn(Arrays.asList(TestUtils.createTestLoan(), TestUtils.createTestLoan()));
        checker.checkMarketplace();
        
        String output = capture.toString();
		assertThat(output, containsString("New loans: [" + TestUtils.getTestLoanString() + ", " + TestUtils.getTestLoanString() + "]"));
	}

}

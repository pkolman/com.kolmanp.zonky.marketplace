package com.kolmanp.zonky.loaders.impl;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.kolmanp.zonky.TestUtils;
import com.kolmanp.zonky.model.Loan;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@RunWith(SpringRunner.class)
@RestClientTest(LoansLoaderImpl.class)
public class LoansLoaderImplTest {

	private static final String SERVER_RESPONSE_WITH_2_LOANS = "[{\"id\":111,\"url\":\"https://app.zonky.cz/loan/111\",\"name\":\"Refinancování půjčky\", \"datePublished\":\"2017-11-18T10:15:48.019+01:00\"}, {\"id\":2222,\"url\":\"https://app.zonky.cz/loan/2222\",\"name\":\"Refinancování\", \"datePublished\":\"2017-11-18T10:22:48.000+01:00\"}]";
	private static final String BASE_URL = "https://api.zonky.cz/loans/marketplace?fields=id,url,datePublished,name";

	@TestConfiguration
    static class ConfigServiceImplTestContextConfiguration {
  
        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
        
    }

	@Autowired
    private LoansLoaderImpl loader;
	
	@Autowired
    private RestTemplate template;
	
	private MockRestServiceServer server;
	
	@Before
    public void setUp() throws Exception {
        server = MockRestServiceServer.createServer(template);
    }
	
	@Test
    public void testNoLoans() throws Exception {
        server.expect(requestTo("https://api.zonky.cz/loans/marketplace?fields=id,url,datePublished,name")).andRespond(withSuccess("[]", MediaType.APPLICATION_JSON));
        List<Loan> loans = loader.getLoans(null, TestUtils.createTestSorting());
        assertEquals(loans, Collections.emptyList());
    }

//	@Test
//    public void testFallback() throws Exception {
//        server.expect(requestTo("https://api.zonky.cz/loans/marketplace?fields=id,url,datePublished,name")).andRespond(MockRestResponseCreators.withStatus(HttpStatus.BAD_REQUEST));
//        List<Loan> loans = loader.getLoans(null, TestUtils.createTestSorting());
//        assertEquals(loans, Collections.emptyList());
//    }
	
	@Test
    public void testManyLoans() throws Exception {
        server.expect(requestTo("https://api.zonky.cz/loans/marketplace?fields=id,url,datePublished,name")).andRespond(withSuccess(SERVER_RESPONSE_WITH_2_LOANS, MediaType.APPLICATION_JSON));
        List<Loan> loans = loader.getLoans(null, TestUtils.createTestSorting());
        assertEquals(loans, Arrays.asList(
        		new Loan(111, "Refinancování půjčky", TestUtils.createDate(2017, 11, 18, 10, 15, 48, 19), "https://app.zonky.cz/loan/111"),
        		new Loan(2222, "Refinancování", TestUtils.createDate(2017, 11, 18, 10, 22, 48, 0), "https://app.zonky.cz/loan/2222")
        		));
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void testNullSorting() throws Exception {
        loader.getLoans(null, null);
    }
	
	@Test
	public void testGetUrlWithoutLastUpdateDate() {
		String url = loader.getUrl(null);
		assertEquals(url, BASE_URL);
	}
	
	@Test
	public void testGetUrlWithLastUpdateDate() {
		String url = loader.getUrl(TestUtils.getTestLastUpdateDate());
		assertEquals(url, BASE_URL + "&datePublished__gt=" + TestUtils.getTestLastUpdateDateString());
	}
}

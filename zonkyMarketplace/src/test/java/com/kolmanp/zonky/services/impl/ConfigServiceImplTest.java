package com.kolmanp.zonky.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.kolmanp.zonky.services.ConfigService;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@RunWith(SpringRunner.class)
public class ConfigServiceImplTest {

	@TestConfiguration
    static class ConfigServiceImplTestContextConfiguration {
  
        @Bean
        public ConfigService configService() {
            return new ConfigServiceImpl();
        }
    }
	
	@Autowired
	private ApplicationContext context;
	
	@Test
	public void testConfigServiceValueHolding() {
		ConfigService config1 = context.getBean(ConfigService.class);
		config1.setLastUpdate(new Date());
		
		ConfigService config2 = context.getBean(ConfigService.class);
		
		assertEquals(config1.getLastUpdate(), config2.getLastUpdate());
	}
	
	@Test
	public void testConfigServiceDateChanges() {
		ConfigService config = context.getBean(ConfigService.class);
		Date date = new Date();
		config.setLastUpdate(date);
		assertTrue(date == config.getLastUpdate());
	}
	
}

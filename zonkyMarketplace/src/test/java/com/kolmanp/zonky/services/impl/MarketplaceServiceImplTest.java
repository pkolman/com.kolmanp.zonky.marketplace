package com.kolmanp.zonky.services.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.kolmanp.zonky.TestUtils;
import com.kolmanp.zonky.model.Loan;
import com.kolmanp.zonky.services.ConfigService;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@RunWith(SpringRunner.class)
public class MarketplaceServiceImplTest {

	@Test(expected = IllegalArgumentException.class)
	public void testLastUpdateNullLoans() {
		new MarketplaceServiceImpl(null, null).setLastUpdate(null);
	}
	
	@Test
	public void testLastUpdateNoLoans() {
		ConfigServiceImpl config = new ConfigServiceImpl();
		Date date = TestUtils.getTestLastUpdateDate();
		config.setLastUpdate(date);
		new MarketplaceServiceImpl(config, null).setLastUpdate(new ArrayList<>());
		
		assertEquals(date, config.getLastUpdate());
	}
	
	@Test
	public void testLastUpdateOneLoan() {
		ConfigServiceImpl config = new ConfigServiceImpl();
		config.setLastUpdate(null);
		new MarketplaceServiceImpl(config, null).setLastUpdate(Arrays.asList(TestUtils.createTestLoan()));
		
		assertEquals(TestUtils.getTestLastUpdateDate(), config.getLastUpdate());
	}
	
	@Test
	public void testSetLastUpdateChanges() {
		Date date1 = new Date();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.DAY_OF_YEAR, 1);
		Date date2 = cal.getTime();
		
		ConfigService config = new ConfigServiceImpl();
		config.setLastUpdate(date1);
		
		Loan loan = new Loan(1, "Test", date2, "http://testurl.test");
		new MarketplaceServiceImpl(config, null).setLastUpdate(Arrays.asList(loan));
		
		assertEquals(date2, config.getLastUpdate());
	}
	
}

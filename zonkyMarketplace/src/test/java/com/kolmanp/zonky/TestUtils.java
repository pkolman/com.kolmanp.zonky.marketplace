package com.kolmanp.zonky;

import java.util.Calendar;
import java.util.Date;

import com.kolmanp.zonky.model.Loan;
import com.kolmanp.zonky.utils.Direction;
import com.kolmanp.zonky.utils.Sorting;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class TestUtils {

	public static Date getTestLastUpdateDate() {
		return createDate(2017, 10, 10, 12, 12, 12, 0);
	}
	
	public static Date createDate(int year, int month, int day, int hour, int minute, int second, int milisecond) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day, hour, minute, second);
		cal.set(Calendar.MILLISECOND, milisecond);
		Date date = cal.getTime(); 
		return date;
	}

	public static String getTestLastUpdateDateString() {
		return "2017-10-10T12:12:12.000";
	}
	
	public static Loan createTestLoan() {
		return new Loan(111, "Test", getTestLastUpdateDate(), "http://testurl.test");
	}

	public static String getTestLoanString() {
		return "Loan [id=111, datePublished=" + getTestLastUpdateDateString() + ", url=http://testurl.test, name=Test]";
	}
	
	public static Sorting createTestSorting() {
		return new Sorting("datePublished", Direction.DESC);
	}

}

package com.kolmanp.zonky.outputwriters.impl;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.kolmanp.zonky.TestUtils;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LogOutputWriterTest {

	@TestConfiguration
    static class LogOutputWriterTestContextConfiguration {
  
        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
        
    }
	
	@Autowired
	private LogOutputWriter writer;
	
	@Rule
    public OutputCapture capture =  new OutputCapture();
    
    @Before
    public void setup() {
    	capture.reset();
    }
    
    @Test
	public void testNoLoans() {
    	writer.write(Collections.emptyList());
    	
        String output = capture.toString();
		assertThat(output, containsString("New loans: []"));
	}
    
    @Test
	public void testOneLoan() {
    	writer.write(Arrays.asList(TestUtils.createTestLoan()));
        
        String output = capture.toString();
		assertThat(output, containsString("New loans: [" + TestUtils.getTestLoanString() + "]"));
	}
    
    @Test
	public void testManyLoans() {
    	writer.write(Arrays.asList(TestUtils.createTestLoan(), TestUtils.createTestLoan()));
        
        String output = capture.toString();
		assertThat(output, containsString("New loans: [" + TestUtils.getTestLoanString() + ", " + TestUtils.getTestLoanString() + "]"));
	}
	
}

package com.kolmanp.zonky.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kolmanp.zonky.loaders.LoansLoader;
import com.kolmanp.zonky.loaders.impl.LoansLoaderImpl;
import com.kolmanp.zonky.model.Loan;
import com.kolmanp.zonky.services.ConfigService;
import com.kolmanp.zonky.services.MarketplaceService;
import com.kolmanp.zonky.utils.Direction;
import com.kolmanp.zonky.utils.Sorting;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@Service
public class MarketplaceServiceImpl implements MarketplaceService {

	private ConfigService config;
	private LoansLoader loader;
	
	@Autowired
	public MarketplaceServiceImpl(ConfigService config, LoansLoader loader) {
		this.config = config;
		this.loader = loader;
	}
	
	@Override
	public List<Loan> getNewLoans() {
		Date lastUpdate = config.getLastUpdate();
		List<Loan> loans = loader.getLoans(lastUpdate, new Sorting(LoansLoaderImpl.DATE_PUBLISHED_PARAM, Direction.DESC));
        setLastUpdate(loans);
        return loans;
	}

	void setLastUpdate(List<Loan> loans) {
		if (loans == null) throw new IllegalArgumentException("Error while input loans parameter is null!!");
		if (loans.isEmpty()) return;
		
		Date lastUpdate = loans.get(0).getDatePublished();
		config.setLastUpdate(lastUpdate);
	}
	
}

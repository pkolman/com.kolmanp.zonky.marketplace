package com.kolmanp.zonky.services.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.kolmanp.zonky.services.ConfigService;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@Service
public class ConfigServiceImpl implements ConfigService {

	private Date lastUpdate;

	@Override
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public Date getLastUpdate() {
		return lastUpdate;
	}

	
	
}

package com.kolmanp.zonky.services;

import java.util.List;

import com.kolmanp.zonky.model.Loan;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public interface MarketplaceService {

	List<Loan> getNewLoans();

}

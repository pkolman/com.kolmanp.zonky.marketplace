package com.kolmanp.zonky.services;

import java.util.Date;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public interface ConfigService {

	void setLastUpdate(Date date);

	Date getLastUpdate();

}

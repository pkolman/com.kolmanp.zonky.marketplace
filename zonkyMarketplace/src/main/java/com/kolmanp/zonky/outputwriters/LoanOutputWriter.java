package com.kolmanp.zonky.outputwriters;

import java.util.List;

import com.kolmanp.zonky.model.Loan;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public interface LoanOutputWriter {

	void write(List<Loan> loans);

}

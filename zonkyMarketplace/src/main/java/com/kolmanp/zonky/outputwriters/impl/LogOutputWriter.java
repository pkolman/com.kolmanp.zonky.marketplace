package com.kolmanp.zonky.outputwriters.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.kolmanp.zonky.model.Loan;
import com.kolmanp.zonky.outputwriters.LoanOutputWriter;
import com.kolmanp.zonky.utils.HasLogger;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@Component
public class LogOutputWriter implements LoanOutputWriter, HasLogger {

	@Override
	public void write(List<Loan> loans) {
		getLogger().info("New loans: " + loans);
	}
	
}

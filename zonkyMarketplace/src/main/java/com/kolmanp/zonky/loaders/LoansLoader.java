package com.kolmanp.zonky.loaders;

import java.util.Date;
import java.util.List;

import com.kolmanp.zonky.model.Loan;
import com.kolmanp.zonky.utils.Sorting;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public interface LoansLoader {
	
	List<Loan> getLoans(Date fromPublicationDate, Sorting sorting);
	
}

package com.kolmanp.zonky.loaders.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.kolmanp.zonky.loaders.LoansLoader;
import com.kolmanp.zonky.model.Loan;
import com.kolmanp.zonky.utils.DateTimeFormatter;
import com.kolmanp.zonky.utils.HasLogger;
import com.kolmanp.zonky.utils.Sorting;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@Component
public class LoansLoaderImpl implements LoansLoader, HasLogger {

	@Autowired
    private RestTemplate template;
	
	public static final String DATE_PUBLISHED_PARAM = "datePublished";

	private static final String BASE_URL = "https://api.zonky.cz/loans/marketplace?fields=id,url,datePublished,name";
	private static final String X_ORDER_HEADER = "X-Order";
	private static final String DATE_PUBLISHED_GREATER_PARAM = DATE_PUBLISHED_PARAM + "__gt";
	
	@HystrixCommand(fallbackMethod = "noLoansFallback")
	@Override
	public List<Loan> getLoans(Date fromPublicationDate, Sorting sorting) {
		if (sorting == null) throw new IllegalArgumentException("Error while sorting is null!!");
		
		String url = getUrl(fromPublicationDate);
		ResponseEntity<List<Loan>> response = template.exchange(url, HttpMethod.GET, new HttpEntity<>(createSortingHeaders(sorting)), new ParameterizedTypeReference<List<Loan>>() {});
		return response.getBody();
	}
	
	@SuppressWarnings("unused") // used as hystrix command
	private List<Loan> noLoansFallback(Date fromPublicationDate, Sorting sorting) {
		getLogger().warn("Connection to external server is down. No loans will be returned.");
		return Collections.emptyList();
	}
	
	String getUrl(Date fromPublicationDate) {
		if (fromPublicationDate == null) return BASE_URL;
		
		return BASE_URL + "&" + DATE_PUBLISHED_GREATER_PARAM + "=" + new DateTimeFormatter().format(fromPublicationDate);
	}
	
	private HttpHeaders createSortingHeaders(Sorting sorting) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set(X_ORDER_HEADER, sorting.toString());
		return httpHeaders;
	}

}

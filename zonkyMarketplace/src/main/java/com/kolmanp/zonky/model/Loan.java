package com.kolmanp.zonky.model;

import java.util.Date;

import com.kolmanp.zonky.utils.DateTimeFormatter;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class Loan {

	private long id;
	private Date datePublished;
	private String url;
	private String name;
	
	@SuppressWarnings("unused") // deserialization
	private Loan() {}
	
	public Loan(long id, String name, Date datePublished, String url) {
		this.id = id;
		this.datePublished = datePublished;
		this.url = url;
		this.name = name;
	}

	public long getId() {
		return id;
	}
	
	public Date getDatePublished() {
		return datePublished;
	}
	
	public String getUrl() {
		return url;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Loan other = (Loan) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Loan [id=" + id + ", datePublished=" + new DateTimeFormatter().format(datePublished) + ", url=" + url + ", name=" + name + "]";
	}
	
}
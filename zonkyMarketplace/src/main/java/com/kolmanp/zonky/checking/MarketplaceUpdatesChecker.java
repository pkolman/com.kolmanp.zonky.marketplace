package com.kolmanp.zonky.checking;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.kolmanp.zonky.model.Loan;
import com.kolmanp.zonky.outputwriters.LoanOutputWriter;
import com.kolmanp.zonky.services.MarketplaceService;
import com.kolmanp.zonky.utils.HasLogger;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
@Component
public class MarketplaceUpdatesChecker implements HasLogger {

	@Autowired
	private MarketplaceService service;
	
	@Autowired
	private List<LoanOutputWriter> writers;
	
	@Scheduled(cron = "0 */1 * * * *")
    public void checkMarketplace() {
        List<Loan> loans = service.getNewLoans();
        writers.forEach(w -> w.write(loans));
    }
	
}

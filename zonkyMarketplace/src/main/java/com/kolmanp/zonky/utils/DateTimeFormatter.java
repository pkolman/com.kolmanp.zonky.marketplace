package com.kolmanp.zonky.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class DateTimeFormatter {

	private static final String DATE_PARAM_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	
	public String format(Date date) {
		if (date == null) throw new IllegalArgumentException("Error while input date cannot be null!!");
		
		return new SimpleDateFormat(DATE_PARAM_FORMAT).format(date);
	}
	
}

package com.kolmanp.zonky.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public interface HasLogger {

	default Logger getLogger() {
		return LoggerFactory.getLogger(this.getClass());
	}
	
}

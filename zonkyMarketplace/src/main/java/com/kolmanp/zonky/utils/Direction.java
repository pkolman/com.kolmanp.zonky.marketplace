package com.kolmanp.zonky.utils;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public enum Direction {
	
	ASC(""), DESC("-");
	
	private String prefix;

	private Direction(String prefix) {
		this.prefix = prefix;
	}
	
	@Override
	public String toString() {
		return prefix;
	}
	
}

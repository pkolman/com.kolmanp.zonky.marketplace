package com.kolmanp.zonky.utils;

/**
 * @author Pavel Kolman, kolmanpav@gmail.com
 */
public class Sorting {

	private final Direction dir;
	private final String feature;
	
	public Sorting(String feature, Direction dir) {
		if (dir == null) throw new IllegalArgumentException("Error while input dir is null!!");
		if (feature == null) throw new IllegalArgumentException("Error while input feature is null!!");
		this.dir = dir;
		this.feature = feature;
	}

	public Direction getDir() {
		return dir;
	}

	public String getFeature() {
		return feature;
	}
	
	@Override
	public String toString() {
		return dir + feature;
	}
	
}
